#include "KeyBank.hpp"

KeyBank::KeyBank(
        CardButton const baseValue,
        uint8_t const pinCount,
        uint8_t const mask,
        KeyBank::BankReader const reader
) : baseValue(baseValue),
    pinCount(pinCount),
    mask(mask),
    reader(reader)
{}

CardButton KeyBank::poll() const
{
    uint8_t const sample = reader() & mask;

    for (uint8_t k = 0; k < pinCount; k++)
    {
        // check the (k)th bit
        auto const pinMask = 1 << (k);
        auto const pinSample = sample & pinMask;

        // Low values are keypresses
        if (!pinSample)
        {
            return static_cast<CardButton>(static_cast<uint8_t>(baseValue) + k);
        }
    }

    return CardButton::NO_KEY;
}
