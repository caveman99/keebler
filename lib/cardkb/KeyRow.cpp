#include "KeyRow.hpp"

KeyRow::KeyRow(int const count, KeyBank * const banks)
: bankCount(count),
  banks(banks)
{}

CardButton KeyRow::pollOne() const
{
    for (int k = 0; k < bankCount; k++)
    {
        CardButton const sample = banks[k].poll();
        if (sample != CardButton::NO_KEY)
        {
            return sample;
        }
    }

    return CardButton::NO_KEY;
}

/*

KeyRow::KeyRow(uint8_t const count, KeyRow::ColumnGroup const groups[])
: groupCount(count),
  groups(groups)
{}

KeyRow::Key KeyRow::pollOne()
{
    for (uint8_t k = 0; k < groupCount; k++)
    {
        KeyRow::ColumnGroup const group = groups[k];
        auto const sample = group.reader() & group.pinMask;

        // Scan the group for keypresses
        for (uint8_t l = 0; l < group.size; l++)
        {
            if ((sample & (1 << (l + group.offset))) == 0)
            {
                return group.indexStart + l;
            }
        }
    }
}
 */
