#include "KeyBuffer.hpp"
#include "Arduino.h"
#include "time.h"

KeyBuffer::KeyBuffer(size_t bufferLength, unsigned char *charBuffer)
    : limit(bufferLength),
      buffer(charBuffer),
      index(0),
      count(0),
      lastUpdateTs(0)
{}

#define MIN(L, R) (((L) <= (R)) ? (L) : (R))
#define MAX(L, R) (((L) >= (R)) ? (L) : (R))

bool KeyBuffer::accept(unsigned char const character)
{
  lastUpdateTs = millis();

  buffer[index % limit] = character;
  index = (index + 1) % limit;
  count = MIN(limit, count + 1);

  return true;
}

unsigned char KeyBuffer::take()
{
  if (count > 0) {
    auto const i = (limit + index - count) % limit;
    count -= 1;
    return buffer[i];
  }
  return 0;
}

unsigned char KeyBuffer::peek() const
{
  if (count > 0)
  {
    return buffer[(limit + index - 1) % limit];
  }
  return 0;
}

bool KeyBuffer::isNotEmpty() const
{
  return count > 0;
}

unsigned long KeyBuffer::lastUpdate() const
{
  return lastUpdateTs;
}
