#include "CardKB.hpp"

#include <time.h> // arduino Time for millis()

CardKB::CardKB(
    GenericSink<unsigned char> *charSink,
    Adafruit_NeoPixel *const statusLight
) : GenericSink<InputEvents::Event>(),
    characterSink(charSink),
    statusLight(statusLight),
    inputEvents(dynamic_cast<GenericSink<InputEvents::Event> *>(this)),
    matrix(&inputEvents),
    mode(Mode::NORMAL),
    activeKey(KeyMatrix::Button::NO_KEY),
    modeEnterTime(0),
    flashUntil(0)
{
}

/*
void CardKB::systemInit(CardKB * instance)
{
    instance->matrix.setup();
    Wire.begin(I2C_ADDRESS);
    // Register globals
    CardKB::currentKB = instance;
    // TODO: where?
    //Wire.onRequest(CardKB::onWireReady);
}
 */

//#define SET_BIT(val, bitn)  (val |=(1<<(bitn)))
//#define CLR_BIT(val, bitn)  (val&=~(1<<(bitn)))
#define GET_BIT(val, bitn)  ((val)  &(1<<(bitn)))

unsigned char const CardKB::KEYMAP[] = {
    //norm, shift,  sym,    fn,
    27, 27, 128, 128,    //esc
    '1', '1', '!', 129,    //1
    '2', '2', '@', 130,    //2
    '3', '3', '#', 131,    //3
    '4', '4', '$', 132,    //4
    '5', '5', '%', 133,    //5
    '6', '6', '^', 134,    //6
    '7', '7', '&', 135,    //7
    '8', '8', '*', 136,    //8
    '9', '9', '(', 137,    //9
    '0', '0', ')', 138,    //0
    8, 127, 8, 139,    //del
    9, 9, 9, 140,    //tab
    'q', 'Q', 0, 141,    //q
    'w', 'W', 0, 142,    //w
    'e', 'E', '[', 143,    //e
    'r', 'R', ']', 144,    //r
    't', 'T', '/', 145,    //t
    'y', 'Y', '\\', 146,    //y
    'u', 'U', '|', 147,    //u
    'i', 'I', '~', 148,    //i
    'o', 'O', '\'', 149,    //o
    'p', 'P', '"', 150,    //p
    0, 0, 0, 0,      //no key
    180, 180, 180, 152,    //LEFT
    181, 181, 181, 153,    //UP
    'a', 'A', ';', 154,    //a
    's', 'S', ':', 155,    //s
    'd', 'D', '`', 156,    //d
    'f', 'F', '+', 157,    //f
    'g', 'G', '-', 158,    //g
    'h', 'H', '_', 159,    //h
    'j', 'J', '=', 160,    //j
    'k', 'K', '?', 161,    //k
    'l', 'L', 0, 162,    //l
    13, 13, 13, 163,    //enter
    182, 182, 182, 164,    //DOWN
    183, 183, 183, 165,    //RIGHT
    'z', 'Z', 0, 166,    //z
    'x', 'X', 0, 167,    //x
    'c', 'C', 0, 168,    //c
    'v', 'V', 0, 169,    //v
    'b', 'B', 0, 170,    //b
    'n', 'N', 0, 171,    //n
    'm', 'M', 0, 172,    //m
    ',', ',', '<', 173,    //,
    '.', '.', '>', 174,    //.
    ' ', ' ', ' ', 175     //space
};

// PB4, pulled low
#define SHIFT_PRESSED   (GET_BIT(PORTB, 4) == 0)
// PB5, pulled low
#define SYM_PRESSED     (GET_BIT(PORTB, 7) == 0)
// PB6, pulled low
#define FN_PRESSED      (GET_BIT(PORTB, 6) == 0)

/*
CardKB::Color const CardKB::COLOR_FLASH = Adafruit_NeoPixel::Color(3, 3, 3);
CardKB::Color const CardKB::COLOR_OFF   = Adafruit_NeoPixel::Color(0, 0, 0);
CardKB::Color const CardKB::COLOR_SHIFT   = Adafruit_NeoPixel::Color(4, 1, 1);
CardKB::Color const CardKB::COLOR_SYM = Adafruit_NeoPixel::Color(1, 4, 1);
CardKB::Color const CardKB::COLOR_FN  = Adafruit_NeoPixel::Color(1, 1, 4);
 */
CardKB::Color const CardKB::COLOR_FLASH = Adafruit_NeoPixel::Color(4, 2, 2);
CardKB::Color const CardKB::COLOR_OFF = Adafruit_NeoPixel::Color(0, 0, 0);
CardKB::Color const CardKB::COLOR_SHIFT = Adafruit_NeoPixel::Color(1, 4, 1);
CardKB::Color const CardKB::COLOR_SYM = Adafruit_NeoPixel::Color(1, 1, 4);
CardKB::Color const CardKB::COLOR_FN = Adafruit_NeoPixel::Color(4, 1, 1);

#define FLASH_MS 300

void CardKB::requestFlash()
{
  flashUntil = millis() + FLASH_MS;
  renderStatus();
}

unsigned char CardKB::mapKey(CardKB::Mode mode, KeyMatrix::Button key)
{
  KeyMatrix::Modifier mod;

  switch (mode)
  {
    case CardKB::Mode::SHIFT:
    case CardKB::Mode::SHIFT_LOCK:mod = KeyMatrix::Modifier::SHIFT;
      break;
    case CardKB::Mode::SYMBOL:
    case CardKB::Mode::SYM_LOCK:mod = KeyMatrix::Modifier::SYM;
      break;

    case CardKB::Mode::FUNCTION:
    case CardKB::Mode::FN_LOCK:mod = KeyMatrix::Modifier::FN;
      break;
    case CardKB::Mode::NORMAL:
    default:mod = KeyMatrix::Modifier::NONE;
  }

  return CardKB::KEYMAP[static_cast<int>(key) * 4 + static_cast<int>(mod)];
}

// Above this number of milliseconds, a user is "holding" the key
#define HOLD_THRESHOLD 3000

void CardKB::poll()
{
  matrix.poll();
}

// Macros for convenience - set the light to a given color (only works in an instance method of CardKB)
#define PAINT(COLOR) statusLight->setPixelColor(0, (COLOR))
// Set the light to a given color IFF the given timestamp is even
#define BLINK(TIME, COLOR) PAINT(((TIME) %2 == 0) ? (COLOR) : CardKB::COLOR_OFF)

void CardKB::renderStatus()
{
  auto now = millis() / 500; // blink period 500ms

  // Flashing requested - just flash on
  if (now < flashUntil)
  {
    PAINT(CardKB::COLOR_FLASH);
    return;
  }

  switch (mode)
  {
    case CardKB::Mode::NORMAL:PAINT(CardKB::COLOR_OFF);
      break;
    case CardKB::Mode::SHIFT:BLINK(now, CardKB::COLOR_SHIFT);
      break;
    case CardKB::Mode::SHIFT_LOCK:PAINT(CardKB::COLOR_SHIFT);
      break;
    case CardKB::Mode::SYMBOL:BLINK(now, CardKB::COLOR_SYM);
      break;
    case CardKB::Mode::SYM_LOCK:PAINT(CardKB::COLOR_SYM);
      break;
    case CardKB::Mode::FUNCTION:BLINK(now, CardKB::COLOR_FN);
      break;
    case CardKB::Mode::FN_LOCK:PAINT(CardKB::COLOR_FN);
      break;
  }

  statusLight->show();
}

bool CardKB::isSticky(CardKB::Mode mode)
{
  switch (mode)
  {
    case CardKB::Mode::FN_LOCK:
    case CardKB::Mode::SHIFT_LOCK:
    case CardKB::Mode::SYM_LOCK:return true;
    case CardKB::Mode::NORMAL:
    case CardKB::Mode::FUNCTION:
    case CardKB::Mode::SHIFT:
    case CardKB::Mode::SYMBOL:
    default:return false;
  }
}

static KeyMatrix::Modifier modifierFor(CardKB::Mode const mode)
{
  switch (mode)
  {
    case CardKB::Mode::SHIFT:
    case CardKB::Mode::SHIFT_LOCK:return KeyMatrix::Modifier::SHIFT;
    case CardKB::Mode::SYMBOL:
    case CardKB::Mode::SYM_LOCK:return KeyMatrix::Modifier::SYM;
    case CardKB::Mode::FUNCTION:
    case CardKB::Mode::FN_LOCK:return KeyMatrix::Modifier::FN;
    default:
      // Should never get here
    case CardKB::Mode::NORMAL:return KeyMatrix::Modifier::NONE;
  }
}

static CardKB::Mode modeFor(KeyMatrix::Modifier const modifier)
{
  switch (modifier)
  {
    case KeyMatrix::Modifier::SHIFT: return CardKB::Mode::SHIFT;
    case KeyMatrix::Modifier::SYM: return CardKB::Mode::SYMBOL;
    case KeyMatrix::Modifier::FN: return CardKB::Mode::FUNCTION;
    default:
    case KeyMatrix::Modifier::NONE: return CardKB::Mode::NORMAL;
  }
}

static CardKB::Mode holdModeFor(KeyMatrix::Modifier const modifier)
{
  switch (modifier)
  {
    case KeyMatrix::Modifier::SHIFT: return CardKB::Mode::SHIFT_LOCK;
    case KeyMatrix::Modifier::SYM: return CardKB::Mode::SYM_LOCK;
    case KeyMatrix::Modifier::FN: return CardKB::Mode::FN_LOCK;
    default:
    case KeyMatrix::Modifier::NONE: return CardKB::Mode::NORMAL;
  }
}

bool CardKB::accept(InputEvents::Event const event)
{
  auto const sampleMode = mode;
  auto const sampleKeyMod = modifierFor(sampleMode);
  auto const sampleKey = activeKey;
  unsigned long interval;

  unsigned char const renderedKey = mapKey(sampleMode, sampleKey);

  switch (event.type)
  {
    case InputEvents::Type::KEY_UP:
      if (renderedKey)
      {
        characterSink->accept(renderedKey);
        if (!isSticky(sampleMode))
        {
          mode = Mode::NORMAL;
          modeEnterTime = event.timestamp;
        }
      }
      break;
    case InputEvents::Type::KEY_PRESS:requestFlash();
      activeKey = event.id.button;
      break;
    case InputEvents::Type::MODIFIER_PRESS:
      // pressed a new modifier - change typing mode
      if (event.id.modifier != sampleKeyMod)
      {
        mode = modeFor(event.id.modifier);
        modeEnterTime = event.timestamp;
      }
      break;
    case InputEvents::Type::MODIFIER_UP:interval = event.timestamp - modeEnterTime;

      // on keyup, if has been held long enough to engage "lock", engage
      if (interval >= HOLD_THRESHOLD)
      {
        mode = holdModeFor(event.id.modifier);
        modeEnterTime = event.timestamp;
      }
      break;
  }

  return true;
}



