#ifndef CardKB_KeyBuffer
#define CardKB_KeyBuffer

#include "GenericSink.hpp"

#include <stddef.h>

class KeyBuffer
    : public GenericSink<unsigned char>
{
public:
  KeyBuffer(size_t, unsigned char *);

  bool accept(unsigned char) override;

  [[nodiscard]] unsigned char take();

  [[nodiscard]] unsigned char peek() const;

  [[nodiscard]] bool isNotEmpty() const;

  [[nodiscard]] unsigned long lastUpdate() const;

private:
  size_t limit;
  unsigned char *buffer;

  size_t index;
  size_t count;
  unsigned long lastUpdateTs;
};

//CardKB_KeyBuffer
#endif