#ifndef KEEBLER_KEYBANK_H
#define KEEBLER_KEYBANK_H

#include <stdint.h>

#include "CardButton.hpp"

class KeyBank {

public:
    typedef uint8_t KeyIndex;

    typedef uint8_t (*BankReader)();

public:
    KeyBank(
            CardButton baseValue,
            uint8_t pinCount,
            uint8_t mask,
            BankReader reader
        );

    CardButton poll() const;

private:
    // First index that this group matches
    CardButton const baseValue;

    // Number of pins used
    uint8_t const pinCount;

    // Bitmask applied to pin bank in case some pins need to be "skipped"
    uint8_t const mask;

    BankReader const reader;
};


#endif //KEEBLER_KEYBANK_H
