#ifndef KEEBLER_CARDBUTTON_H
#define KEEBLER_CARDBUTTON_H

/*
 * From the schematic provided by "@Bubba_Nel#8319" 2/22/2023
 *         d0  d1  d2  d3  d4  d5  d6  d7  b0  b1  b2  b3  b4  b6  b7
 * c3:     esc 1   2   3   4   5   6   7   8   9   0   del
 * c2:     tab q   w   e   r   t   y   u   i   o   p
 * c1:     lt  up  a   s   d   f   g   h   j   k   l   cr
 * c0:     dn  rt  z   x   c   v   b   n   m   ,   .   spc
 * gnd:                                                    shf fn  sym
 */
// All the buttons on the matrix
typedef enum class CardButton
{
ESCAPE = 0, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, ZERO, DEL,
TAB,              Q, W, E, R, T, Y, U, I, O, P,
// Placed after 'P' because there is no key on [C2,B3], this is a sentinel elsewhere
NO_KEY,
LEFT, UP,         A, S, D, F, G, H, J, K, L, ENTER,
DOWN, RIGHT,      Z, X, C, V, B, N, M, COMMA, PERIOD, SPACE,
} CardButton;

#endif //KEEBLER_CARDBUTTON_H
