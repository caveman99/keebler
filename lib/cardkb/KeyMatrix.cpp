#include "KeyMatrix.hpp"

#include "Arduino.h"

#include "CardKB.hpp"
#include <time.h>

KeyMatrix::KeyMatrix(GenericSink<Status> *const statusSink)
    : statusSink(statusSink),
      isSetup(false),
      currentStatus({
                        .mod = KeyMatrix::Modifier::NONE,
                        .key = KeyMatrix::Button::NO_KEY,
                    })
{}

/**
 * Set up pins for CardKB v1.1
 * From the schematic provided by "@Bubba_Nel#8319" 2/22/2023
 *         d0  d1  d2  d3  d4  d5  d6  d7  b0  b1  b2  b3  b4  b6  b7
 * c3:     esc 1   2   3   4   5   6   7   8   9   0   del
 * c2:     tab q   w   e   r   t   y   u   i   o   p
 * c1:     lt  up  a   s   d   f   g   h   j   k   l   cr
 * c0:     dn  rt  z   x   c   v   b   n   m   ,   .   spc
 * gnd:                                                    shf fn  sym
 */

void KeyMatrix::setup()
{
  if (!isSetup)
  {
    // C0-3 used as rows: outputs
    DDRC |= 0b1111;
    // Pull rows high
    PORTC |= 0b1111;

#ifdef DEBUG_SERIAL
    // D0, D3-7 used as columns, input on these
    // D1 is TX; D2 is RX for hardware serial
    DDRD &= 0b0000110;
    // Pull all but D1, D2 high
    PORTD |= 0b11111001;

#else
    // D0-7 used as columns: all input
    DDRD = 0b00000000;
    // Pull high
    PORTD = 0b11111111;
#endif

    // B0-3 are columns: set input; B4, B6, B7 are input, too (leave B5 alone)
    DDRB &= 0b00100000;
    // Pull inputs high
    PORTB |= 0b11011111;

    isSetup = true;
  }
}

#define PIN_MASK_STATE(BANK, MASK) (((BANK) & (MASK)) != (MASK))
#define PINB_MASK_STATE(MASK) (PIN_MASK_STATE(PINB, MASK))
#define MASK_FOR(N) (0b1 << (N))
#define PINB_STATE(N) (PINB_MASK_STATE(MASK_FOR(N)))

#define DOING_SHIFT (PINB_STATE(4))
#define DOING_FN    (PINB_STATE(6))
#define DOING_SYM   (PINB_STATE(7))

#ifdef DEBUG_SERIAL
#define PIND_MASK 0b11111001
#else
#define PIND_MASK 0b11111111
#endif

#define PIND_ACTIVE(N) ((0b11111111 - MASK_FOR(N)) & PIND_MASK)
#define PINB_ACTIVE(N) (0b1111 - MASK_FOR(N))

// ROW = 0 means select C0's row (bottom), counting up from bottom
#define PORTC_ROW_SELECT(ROW) ((PORTC & 0xf0) | (0xf - MASK_FOR(ROW)))

#define ROW_SETTLE_TIME_MS 1

bool KeyMatrix::pollBottomRow()
{
  // to read bottom row, pull C0 row low while C1-3 go high
  PORTC = PORTC_ROW_SELECT(0);
  delay(ROW_SETTLE_TIME_MS);

  switch (PIND & PIND_MASK)
  {
    // D0
    case PIND_ACTIVE(0):currentStatus.key = KeyMatrix::Button::DOWN;
      return true;
#ifdef DEBUG_SERIAL
      // Skip D1, D2 - these are UART0
#else
      // D1
    case PIND_ACTIVE(1):currentStatus.key = KeyMatrix::Button::RIGHT;
      return true;
      // D2
    case PIND_ACTIVE(2):currentStatus.key = KeyMatrix::Button::Z;
      return true;
#endif
      // D3
    case PIND_ACTIVE(3):currentStatus.key = KeyMatrix::Button::X;
      return true;
      // D4
    case PIND_ACTIVE(4):currentStatus.key = KeyMatrix::Button::C;
      return true;
      // D5
    case PIND_ACTIVE(5):currentStatus.key = KeyMatrix::Button::V;
      return true;
      // D6
    case PIND_ACTIVE(6):currentStatus.key = KeyMatrix::Button::B;
      return true;
      // D7
    case PIND_ACTIVE(7):currentStatus.key = KeyMatrix::Button::N;
      return true;
  }

  // PB0-3 only are used
  switch (PINB & 0b1111)
  {
    case PINB_ACTIVE(0):currentStatus.key = KeyMatrix::Button::M;
      return true;
    case PINB_ACTIVE(1):currentStatus.key = KeyMatrix::Button::COMMA;
      return true;
    case PINB_ACTIVE(2):currentStatus.key = KeyMatrix::Button::PERIOD;
      return true;
    case PINB_ACTIVE(3):currentStatus.key = KeyMatrix::Button::SPACE;
      return true;
  }

  return false;
}

bool KeyMatrix::pollSecondRow()
{
  // to read 2nd row, pull C1 row low while C0,C2-3 go high
  PORTC = PORTC_ROW_SELECT(1);
  delay(ROW_SETTLE_TIME_MS);

  switch (PIND & PIND_MASK)
  {
    // D0
    case PIND_ACTIVE(0):currentStatus.key = KeyMatrix::Button::LEFT;
      return true;
#ifdef DEBUG_SERIAL
      // Skip D1, D2 - these are UART0
#else
      // D1
    case PIND_ACTIVE(1):currentStatus.key = KeyMatrix::Button::UP;
      return true;
      // D2
    case PIND_ACTIVE(2):currentStatus.key = KeyMatrix::Button::A;
      return true;
#endif
      // D3
    case PIND_ACTIVE(3):currentStatus.key = KeyMatrix::Button::S;
      return true;
      // D4
    case PIND_ACTIVE(4):currentStatus.key = KeyMatrix::Button::D;
      return true;
      // D5
    case PIND_ACTIVE(5):currentStatus.key = KeyMatrix::Button::F;
      return true;
      // D6
    case PIND_ACTIVE(6):currentStatus.key = KeyMatrix::Button::G;
      return true;
      // D7
    case PIND_ACTIVE(7):currentStatus.key = KeyMatrix::Button::H;
      return true;
  }

  // PB0-3 only are used
  switch (PINB & 0b1111)
  {
    case PINB_ACTIVE(0):currentStatus.key = KeyMatrix::Button::J;
      return true;
    case PINB_ACTIVE(1):currentStatus.key = KeyMatrix::Button::K;
      return true;
    case PINB_ACTIVE(2):currentStatus.key = KeyMatrix::Button::L;
      return true;
    case PINB_ACTIVE(3):currentStatus.key = KeyMatrix::Button::ENTER;
      return true;
  }


  return false;
}

bool KeyMatrix::pollThirdRow()
{
  // to read third row, pull C2 row low while C0-1,C3 go high
  PORTC = PORTC_ROW_SELECT(2);
  delay(ROW_SETTLE_TIME_MS);

  switch (PIND & PIND_MASK)
  {
    // D0
    case PIND_ACTIVE(0):currentStatus.key = KeyMatrix::Button::TAB;
      return true;
#ifdef DEBUG_SERIAL
      // Skip D1, D2 - these are UART0
#else
      // D1
    case PIND_ACTIVE(1):currentStatus.key = KeyMatrix::Button::Q;
      return true;
      // D2
    case PIND_ACTIVE(2):currentStatus.key = KeyMatrix::Button::W;
      return true;
#endif
      // D3
    case PIND_ACTIVE(3):currentStatus.key = KeyMatrix::Button::E;
      return true;
      // D4
    case PIND_ACTIVE(4):currentStatus.key = KeyMatrix::Button::R;
      return true;
      // D5
    case PIND_ACTIVE(5):currentStatus.key = KeyMatrix::Button::T;
      return true;
      // D6
    case PIND_ACTIVE(6):currentStatus.key = KeyMatrix::Button::Y;
      return true;
      // D7
    case PIND_ACTIVE(7):currentStatus.key = KeyMatrix::Button::U;
      return true;
  }

  // PB0-3 only are used
  switch (PINB & 0b1111)
  {
    case PINB_ACTIVE(0):currentStatus.key = KeyMatrix::Button::I;
      return true;
    case PINB_ACTIVE(1):currentStatus.key = KeyMatrix::Button::O;
      return true;
    case PINB_ACTIVE(2):currentStatus.key = KeyMatrix::Button::P;
      return true;
  }

  return false;
}

bool KeyMatrix::pollTopRow()
{
  // to read top row, pull C3 row low while C0-2 go high
  PORTC = PORTC_ROW_SELECT(3);
  delay(ROW_SETTLE_TIME_MS);

  switch (PIND & PIND_MASK)
  {
    // D0
    case PIND_ACTIVE(0):currentStatus.key = KeyMatrix::Button::ESCAPE;
      return true;
#ifdef DEBUG_SERIAL
      // Skip D1, D2 - these are UART0
#else
      // D1
    case PIND_ACTIVE(1):currentStatus.key = KeyMatrix::Button::ONE;
      return true;
      // D2
    case PIND_ACTIVE(2):currentStatus.key = KeyMatrix::Button::TWO;
      return true;
#endif
      // D3
    case PIND_ACTIVE(3):currentStatus.key = KeyMatrix::Button::THREE;
      return true;
      // D4
    case PIND_ACTIVE(4):currentStatus.key = KeyMatrix::Button::FOUR;
      return true;
      // D5
    case PIND_ACTIVE(5):currentStatus.key = KeyMatrix::Button::FIVE;
      return true;
      // D6
    case PIND_ACTIVE(6):currentStatus.key = KeyMatrix::Button::SIX;
      return true;
      // D7
    case PIND_ACTIVE(7):currentStatus.key = KeyMatrix::Button::SEVEN;
      return true;
  }

  // PB0-3 only are used
  switch (PINB & 0b1111)
  {
    case PINB_ACTIVE(0):currentStatus.key = KeyMatrix::Button::EIGHT;
      return true;
    case PINB_ACTIVE(1):currentStatus.key = KeyMatrix::Button::NINE;
      return true;
    case PINB_ACTIVE(2):currentStatus.key = KeyMatrix::Button::ZERO;
      return true;
    case PINB_ACTIVE(3):currentStatus.key = KeyMatrix::Button::DEL;
      return true;
  }

  return false;
}

KeyMatrix::Status KeyMatrix::poll()
{
  setup();

  auto const modSample = currentStatus.mod;
  auto const keySample = currentStatus.key;

  currentStatus.timestamp = millis();

  if (DOING_SHIFT)
  {
    currentStatus.mod = Modifier::SHIFT;
  }
  else if (DOING_SYM)
  {
    currentStatus.mod = Modifier::SYM;
  }
  else if (DOING_FN)
  {
    currentStatus.mod = Modifier::FN;
  }
  else
  {
    currentStatus.mod = Modifier::NONE;
  }

  if (!pollTopRow() && !pollThirdRow() && !pollSecondRow() && !pollBottomRow())
  {
    currentStatus.key = Button::NO_KEY;
  }

  statusSink->accept(currentStatus);
  return currentStatus;
}
