#include "InputEvents.hpp"

//#include "Logger.hpp"

#define MOD_DEBOUNCE_MS 100
#define KEY_DEBOUNCE_MS 20

InputEvents::InputEvents(GenericSink<InputEvents::Event> *const sink)
    : sink(sink),
      pendingKey(KeyMatrix::Button::NO_KEY),
      currentKey(KeyMatrix::Button::NO_KEY),
      keyStartTime(0),
      pendingMod(KeyMatrix::Modifier::NONE),
      currentMod(KeyMatrix::Modifier::NONE),
      modStartTime(0),
      currentEvent({
                       .type = Type::KEY_PRESS,
                       .id = InputEvents::KeyId{.button = KeyMatrix::Button::NO_KEY},
                       .timestamp = 0,
                   })
{
  currentEvent.timestamp = 0;
  currentEvent.type = Type::KEY_PRESS;
  currentEvent.id.button = KeyMatrix::Button::NO_KEY;
}

bool InputEvents::accept(KeyMatrix::Status const event)
{
  Time const sampleTime = event.timestamp;

  if (pendingMod != event.mod)
  {
    pendingMod = event.mod;
    modStartTime = sampleTime;
  }

  auto const modPendingSample = pendingMod;
  auto const modCurrentSample = currentMod;

  if (modPendingSample != modCurrentSample)
  {
    auto intervalStart = modStartTime;
    Time const pendingInterval = sampleTime - intervalStart;

    if (pendingInterval >= MOD_DEBOUNCE_MS)
    {
      currentMod = modPendingSample;
      currentEvent.timestamp = sampleTime;

      if (modPendingSample == KeyMatrix::Modifier::NONE)
      {
        currentEvent.type = Type::MODIFIER_UP;
        currentEvent.id.modifier = modCurrentSample;
      }
      else
      {
        currentEvent.type = Type::MODIFIER_PRESS;
        currentEvent.id.modifier = modPendingSample;
      }

      sink->accept(currentEvent);
    }
  }

  if (pendingKey != event.key)
  {
    pendingKey = event.key;
    keyStartTime = sampleTime;
  }

  auto const pendingKeySample = pendingKey;
  auto const currentKeySample = currentKey;

  if (pendingKeySample != currentKeySample)
  {
    auto const pendingInterval = sampleTime - keyStartTime;

    if (pendingInterval >= KEY_DEBOUNCE_MS)
    {
      currentKey = pendingKeySample;
      currentEvent.timestamp = sampleTime;

      if (pendingKeySample == KeyMatrix::Button::NO_KEY)
      {
        currentEvent.type = Type::KEY_UP;
        currentEvent.id.button = currentKeySample;
      }
      else
      {
        currentEvent.type = Type::KEY_PRESS;
        currentEvent.id.button = pendingKeySample;
      }

      // can't keyup/keydown on NO_KEY
      if (currentEvent.id.button != KeyMatrix::Button::NO_KEY)
      {
        sink->accept(currentEvent);
      }
    }
  }
}
