Version 0.0.1
================================================================================

Initial release of Keebler, the buffered firmware for the M5Stack CardKB.
Performance should be as good or better than the stock firmware. Delays are at
1ms after pulling our row IO's down vs 1ms on stock, but there is at least 20ms
of "debounce" lag on any keypress, so responsiveness vs stock will be mostly 
better and sometimes worse, depending on host performance.

The status LED does not work.

