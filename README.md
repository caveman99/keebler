# Keebler

## An Alternative firmware for the m5stack CardKB

Keebler is a reimlpementation of the firmware for the [M5Stack CardKB](https://docs.m5stack.com/en/unit/cardkb), a
relatively inexpensive, compact keyboard with a [Grove](https://wiki.seeedstudio.com/Grove_System) connector that
accepts up to 5v power (and at least 2.7v, it seems) and exposes two GPIO that are configured to join an i2c bus as a
client device. Since the
[stock firmware](https://github.com/m5stack/M5-ProductExampleCodes/blob/master/Unit/CARDKB/firmware_328p/CardKeyBoard/CardKeyBoard.ino)
has not been updated since a major firmware revision replaced the original atmega328p with an atmega8a, though, and it
does not buffer input, leading to dropped characters on i2c hosts that fail to poll before the on-device state has been
updated with a new keypress.

Keebler is a rewrite of the firmware in C++ using the Arduino framework through [PlatformIO](https://platformio.org/).
It improves on the original firmware by adding a modest buffer for user input along with new polling logic. The stock
firmware only holds the latest character entered by the user in-memory, flushing it to i2c when requested. When user
input occurs with data that has not yet been flushed, it is overwritten, leading to lost keystrokes. By instead placing
characters in a [circular buffer](https://en.wikipedia.org/wiki/Circular_buffer), user input is preserved, allowing
"slower" i2c hosts to "catch up".

## Building

Obtain [PlatformIO](https://platformio.org/) either through `pip` or as a plugin in your
[IDE](https://platformio.org/install/ide?install=vscode) of
[choice](https://docs.platformio.org/en/latest/integration/ide/clion.html). Compliation should be relatively
straightforward:

```
pio run -v
```

This will download the crosscompilers and anything necessary along with build dependencies. Usually, a flashable
firmware will be available at the path `.pio/build/ATmega8/firmware.hex`. PlatformIO will also usually mention it in
the last line of build output:

```
...
.debug_abbrev               1262         0
.debug_line                 1320         0
.debug_str                   378         0
Total                      12733
avr-objcopy -O ihex -R .eeprom .pio/build/ATmega8/firmware.elf .pio/build/ATmega8/firmware.hex
===================================================================================================== [SUCCESS] Took 0.83 seconds =====================================================================================================

Environment    Status    Duration
-------------  --------  ------------
ATmega8        SUCCESS   00:00:00.831
SerialDebug    IGNORED
```

## Flashing

The current configuration in the `platformio.ini` is for a [USBTinyISP](https://learn.adafruit.com/usbtinyisp/overview).
To build from source and flash directly, connect to the ISP port at the bottom of the board (see the
[official documentation](https://docs.m5stack.com/en/unit/cardkb) for more information on the AVRISP port). To ask
PlatformIO to flash a keyboard from the command line, simply run:

```
pio run -t upload
```

Alternately, use the GUI "upload" button in your IDE.

To flash a precompiled hex file, one needs [avrdude](https://www.nongnu.org/avrdude/). The invocation for a firmware
file with the name `firmware.hex` for the usbtinyisp is such:

```
avrdude -v  -c usbtiny -p atmega8a -U flash:w:firmware.hex:i
```

This will flash the firmware to the keyboard and ensure that it is present.

## Backup & Restore

Saving the most important part for last, it can be helpful to back up your stock firmware before flashing keebler onto 
your keyboard since the original vendor has not published it in any form. To accomplish this, simply ask avrdude to 
read it from the attached device:

```
avrdude -v -c usbtiny -p atmega8a -U flash:r:firmware_backup.hex:i
```

This will save the contents of the flash to `firmware_backup.hex`. This can be flashed back with avrdude similarly to 
the above instructions for flashing keebler:

```
avrdude -v -c usbtiny -p atmega8a -U flash:w:firmware_backup.hex:i
```

This puts the stock firmware back where it was found, verifying the operation.
